Template.post.helpers({
    "posts": function () {
        //console.log(Posts.find().fetch());
        return Posts.find({});
    }
});

Template.friends.helpers({
    listUser: function () {
        return Meteor.users.find({
            _id: {
                $ne: Meteor.userId()
            }
        }, {
            sort: {
                'profile.firstname': 1
            }
        });
    }
});

Template.usernames.helpers({
    "usernames": function () {
        console.log(Meteor.users.find({}).fetch());
        return Meteor.users.find({});
    }
});